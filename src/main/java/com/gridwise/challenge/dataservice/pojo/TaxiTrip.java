package com.gridwise.challenge.dataservice.pojo;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table
public class TaxiTrip {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int tripId;
	@Column
	private int vendorId;
	@Column
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy hh:mm:ss a", timezone = "America/New York")
	private Timestamp pickupTime;
	@Column
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy hh:mm:ss a", timezone = "America/New York")
	private Timestamp dropoffTime;
	@Column
	private Integer passengerCount;
	@Column
	private double tripDistance;
	@Column
	private int rateCodeId;
	@Column
	private String storeAndForwardFlag;
	@Column
	private int pickupLocationId;
	@Column
	private int dropoffLocationId;
	@Column
	private int paymentType;
	@Column
	private Double fareAmount;
	@Column
	private Double extra;
	@Column
	private Double tax;
	@Column
	private Double tipAmount;
	@Column
	private Double tollsAmount;
	@Column
	private Double improvementSurcharge;
	@Column
	private Double totalAmount;

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public int getVendorId() {
		return vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

	public Timestamp getPickupTime() {
		return pickupTime;
	}

	public void setPickupTime(Timestamp pickupTime) {
		this.pickupTime = pickupTime;
	}

	public Timestamp getDropoffTime() {
		return dropoffTime;
	}

	public void setDropoffTime(Timestamp dropoffTime) {
		this.dropoffTime = dropoffTime;
	}

	public Integer getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(Integer passengerCount) {
		this.passengerCount = passengerCount;
	}

	public double getTripDistance() {
		return tripDistance;
	}

	public void setTripDistance(double tripDistance) {
		this.tripDistance = tripDistance;
	}

	public int getRateCodeId() {
		return rateCodeId;
	}

	public void setRateCodeId(int rateCodeId) {
		this.rateCodeId = rateCodeId;
	}

	public String getStoreAndForwardFlag() {
		return storeAndForwardFlag;
	}

	public void setStoreAndForwardFlag(String storeAndForwardFlag) {
		this.storeAndForwardFlag = storeAndForwardFlag;
	}

	public int getPickupLocationId() {
		return pickupLocationId;
	}

	public void setPickupLocationId(int pickupLocationId) {
		this.pickupLocationId = pickupLocationId;
	}

	public int getDropoffLocationId() {
		return dropoffLocationId;
	}

	public void setDropoffLocationId(int dropoffLocationId) {
		this.dropoffLocationId = dropoffLocationId;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public Double getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(Double fareAmount) {
		this.fareAmount = fareAmount;
	}

	public Double getExtra() {
		return extra;
	}

	public void setExtra(Double extra) {
		this.extra = extra;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public Double getTipAmount() {
		return tipAmount;
	}

	public void setTipAmount(Double tipAmount) {
		this.tipAmount = tipAmount;
	}

	public Double getTollsAmount() {
		return tollsAmount;
	}

	public void setTollsAmount(Double tollsAmount) {
		this.tollsAmount = tollsAmount;
	}

	public Double getImprovementSurcharge() {
		return improvementSurcharge;
	}

	public void setImprovementSurcharge(Double improvementSurcharge) {
		this.improvementSurcharge = improvementSurcharge;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
}
