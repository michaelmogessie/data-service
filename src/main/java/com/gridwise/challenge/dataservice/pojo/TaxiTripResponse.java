package com.gridwise.challenge.dataservice.pojo;

public class TaxiTripResponse {
	private int responseCode;
	private Object responseBody;

	public static final String TAXI_TRIP_RESPONSE_TEXT_SUCCESS = "SUCCESS";
	public static final String TAXI_TRIP_RESPONSE_TEXT_ERROR = "ERROR";
	public static final int TAXI_TRIP_RESPONSE_CODE_SUCCESS = 200;
	public static final int TAXI_TRIP_RESPONSE_CODE_ERROR = 500;

	public TaxiTripResponse(int responseCode, Object responseBody) {
		super();
		this.responseCode = responseCode;
		this.responseBody = responseBody;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public Object getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(Object responseBody) {
		this.responseBody = responseBody;
	}

}
