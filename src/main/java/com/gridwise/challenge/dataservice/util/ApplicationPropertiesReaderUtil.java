package com.gridwise.challenge.dataservice.util;

import java.io.IOException;
import java.util.Properties;

public class ApplicationPropertiesReaderUtil {
	public static Properties readApplicationPropertiesFile(String applicationPropertiesFileName) throws IOException {
		Properties applicationProperties = new Properties();
		applicationProperties.load(ApplicationPropertiesReaderUtil.class.getClassLoader()
				.getResourceAsStream(applicationPropertiesFileName));
		return applicationProperties;
	}
}
