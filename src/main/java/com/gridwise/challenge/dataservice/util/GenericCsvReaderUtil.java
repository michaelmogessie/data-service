package com.gridwise.challenge.dataservice.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;

public class GenericCsvReaderUtil {
	private static final CsvMapper CSV_MAPPER = new CsvMapper();

	public static <T> List<T> readCsv(Class<T> pojo, InputStream inputStream) throws IOException {
		return CSV_MAPPER.readerFor(pojo).with(CSV_MAPPER.schemaFor(pojo).withHeader().withColumnReordering(true))
				.<T>readValues(inputStream).readAll();
	}

}
