package com.gridwise.challenge.dataservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gridwise.challenge.dataservice.pojo.TaxiTrip;

public interface TaxiTripRepositorySD extends JpaRepository<TaxiTrip, Integer> {
	@Query("select round(avg(tt.totalAmount / tt.tripDistance), 2) from TaxiTrip tt where tt.pickupLocationId=:pickupLocationId and hour(tt.pickupTime) >=:periodStart and hour(tt.pickupTime) <=:periodEnd")
	public Double getAverageFarePerMileForPickupLocationAndTimePeriod(@Param("pickupLocationId") int pickupLocationId,
			@Param("periodStart") int periodStart, @Param("periodEnd") int periodEnd);

	@Query("select round(avg(tt.totalAmount / tt.tripDistance), 2) from TaxiTrip tt where tt.pickupLocationId=:pickupLocationId and dayofweek(tt.pickupTime) =:dayOfWeek")
	public Double getAverageFarePerMileForPickupLocationAndDayOfWeek(@Param("pickupLocationId") int pickupLocationId,
			@Param("dayOfWeek") int dayOfWeek);

	@Query("select round(avg(tt.totalAmount / time_to_sec(timediff(tt.dropoffTime, tt.pickupTime)) / 60), 4) from  TaxiTrip tt where tt.pickupLocationId=:pickupLocationId and hour(tt.pickupTime) >=:periodStart and hour(tt.pickupTime) <=:periodEnd")
	public Double getAverageFarePerMinuteForPickupLocationAndTimePeriod(@Param("pickupLocationId") int pickupLocationId,
			@Param("periodStart") int periodStart, @Param("periodEnd") int periodEnd);

	@Query("select round(avg(tt.totalAmount / time_to_sec(timediff(tt.dropoffTime, tt.pickupTime)) / 60), 4) from TaxiTrip tt where tt.pickupLocationId=:pickupLocationId and dayofweek(tt.pickupTime) =:dayOfWeek")
	public Double getAverageFarePerMinuteForPickupLocationAndDayOfWeek(@Param("pickupLocationId") int pickupLocationId,
			@Param("dayOfWeek") int dayOfWeek);
}
