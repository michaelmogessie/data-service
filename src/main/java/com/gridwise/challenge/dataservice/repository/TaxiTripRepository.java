package com.gridwise.challenge.dataservice.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gridwise.challenge.dataservice.pojo.TaxiTrip;

public class TaxiTripRepository implements ITaxiTripRepository {

	@Autowired
	private TaxiTripRepositorySD taxiTripRepositorySD;

	@Override
	public TaxiTrip addTaxiTrip(TaxiTrip taxiTrip) throws Exception {
		return taxiTripRepositorySD.save(taxiTrip);
	}

	@Override
	public TaxiTrip getTaxiTrip(int taxiTripId) throws Exception {
		return taxiTripRepositorySD.findById(taxiTripId).get();
	}

	@Override
	public Double getAverageFarePerMileForPickupLocationAndTimePeriod(int pickupLocationId, int periodStart,
			int periodEnd) {
		return taxiTripRepositorySD.getAverageFarePerMileForPickupLocationAndTimePeriod(pickupLocationId, periodStart,
				periodEnd);
	}

	@Override
	public Double getAverageFarePerMileForPickupLocationAndDayOfWeek(int pickupLocationId, int dayOfWeek) {
		return taxiTripRepositorySD.getAverageFarePerMileForPickupLocationAndDayOfWeek(pickupLocationId, dayOfWeek);
	}

	@Override
	public Double getAverageFarePerMinuteForPickupLocationAndTimePeriod(int pickupLocationId, int periodStart,
			int periodEnd) {
		return taxiTripRepositorySD.getAverageFarePerMinuteForPickupLocationAndTimePeriod(pickupLocationId, periodStart,
				periodEnd);
	}

	@Override
	public Double getAverageFarePerMinuteForPickupLocationAndDayOfWeek(int pickupLocationId, int dayOfWeek) {
		return taxiTripRepositorySD.getAverageFarePerMinuteForPickupLocationAndDayOfWeek(pickupLocationId, dayOfWeek);
	}

	@Override
	public void uploadTaxiTrips(List<TaxiTrip> taxiTrips) throws Exception {
		taxiTripRepositorySD.saveAll(taxiTrips);
	}

}
