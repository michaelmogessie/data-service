package com.gridwise.challenge.dataservice.repository;

import java.util.List;

import com.gridwise.challenge.dataservice.pojo.TaxiTrip;

public interface ITaxiTripRepository {
	TaxiTrip addTaxiTrip(TaxiTrip taxiTrip) throws Exception;

	TaxiTrip getTaxiTrip(int taxiTripId) throws Exception;

	Double getAverageFarePerMileForPickupLocationAndTimePeriod(int pickupLocationId, int periodStart, int periodEnd);

	Double getAverageFarePerMileForPickupLocationAndDayOfWeek(int pickupLocationId, int dayOfWeek);

	Double getAverageFarePerMinuteForPickupLocationAndTimePeriod(int pickupLocationId, int periodStart, int periodEnd);

	Double getAverageFarePerMinuteForPickupLocationAndDayOfWeek(int pickupLocationId, int dayOfWeek);

	void uploadTaxiTrips(List<TaxiTrip> taxiTrips) throws Exception;

}
