package com.gridwise.challenge.dataservice.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.gridwise.challenge.dataservice.business.ITaxiTripBusiness;
import com.gridwise.challenge.dataservice.pojo.TaxiTrip;
import com.gridwise.challenge.dataservice.pojo.TaxiTripResponse;
import com.gridwise.challenge.dataservice.util.ApplicationPropertiesReaderUtil;

@RestController
public class TaxiTripController {
	@Autowired
	private ITaxiTripBusiness taxiTripBusiness;
	@Autowired
	private Logger dataServiceLogger;

	private final String LANGUAGE_FILE_NAME = "taxitripdataservice.i18n.en.properties";

	@RequestMapping(method = RequestMethod.POST, value = "/addtaxitrip")
	@CrossOrigin()
	public TaxiTripResponse addTaxiTrip(@RequestBody TaxiTrip taxiTrip) {
		try {
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_SUCCESS,
					taxiTripBusiness.addTaxiTrip(taxiTrip));
		} catch (Exception e) {
			dataServiceLogger.error(e.getMessage(), e);
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_ERROR,
					TaxiTripResponse.TAXI_TRIP_RESPONSE_TEXT_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/uploadtaxitrips", consumes = "multipart/form-data")
	@CrossOrigin()
	public TaxiTripResponse uploadTaxiTrips(@RequestParam("file") MultipartFile taxiTripCsvFile) {
		try {
			taxiTripBusiness.uploadTaxiTrips(taxiTripCsvFile.getInputStream());
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_SUCCESS,
					ApplicationPropertiesReaderUtil.readApplicationPropertiesFile(LANGUAGE_FILE_NAME)
							.get("taxitripdataservice.response.csvupload.successful"));
		} catch (Exception e) {
			dataServiceLogger.error(e.getMessage(), e);
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_ERROR,
					TaxiTripResponse.TAXI_TRIP_RESPONSE_TEXT_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/gettaxitrip")
	@CrossOrigin()
	public TaxiTripResponse getTaxiTrip(@RequestParam(value = "taxiTripId") int taxiTripId) {
		try {
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_SUCCESS,
					taxiTripBusiness.getTaxiTrip(taxiTripId));
		} catch (Exception e) {
			dataServiceLogger.error(e.getMessage(), e);
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_ERROR,
					TaxiTripResponse.TAXI_TRIP_RESPONSE_TEXT_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getaveragefarepermileforpickuplocationandtimeperiod")
	@CrossOrigin()
	public TaxiTripResponse getAverageFarePerMileForPickupLocationAndTimePeriod(
			@RequestParam(value = "pickupLocationId") int pickupLocationId,
			@RequestParam(value = "periodStart") int periodStart, @RequestParam(value = "periodEnd") int periodEnd) {
		try {
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_SUCCESS, taxiTripBusiness
					.getAverageFarePerMileForPickupLocationAndTimePeriod(pickupLocationId, periodStart, periodEnd));
		} catch (Exception e) {
			dataServiceLogger.error(e.getMessage(), e);
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_ERROR,
					TaxiTripResponse.TAXI_TRIP_RESPONSE_TEXT_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getaveragefarepermileforpickuplocationanddayofweek")
	@CrossOrigin()
	public TaxiTripResponse getAverageFarePerMileForPickupLocationAndDayOfWeek(
			@RequestParam(value = "pickupLocationId") int pickupLocationId,
			@RequestParam(value = "dayOfWeek") int dayOfWeek) {
		try {
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_SUCCESS,
					taxiTripBusiness.getAverageFarePerMileForPickupLocationAndDayOfWeek(pickupLocationId, dayOfWeek));
		} catch (Exception e) {
			dataServiceLogger.error(e.getMessage(), e);
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_ERROR,
					TaxiTripResponse.TAXI_TRIP_RESPONSE_TEXT_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getaveragefareperminuteforpickuplocationandtimeperiod")
	@CrossOrigin()
	public TaxiTripResponse getAverageFarePerMinuteForPickupLocationAndTimePeriod(
			@RequestParam(value = "pickupLocationId") int pickupLocationId,
			@RequestParam(value = "periodStart") int periodStart, @RequestParam(value = "periodEnd") int periodEnd) {
		try {
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_SUCCESS, taxiTripBusiness
					.getAverageFarePerMinuteForPickupLocationAndTimePeriod(pickupLocationId, periodStart, periodEnd));
		} catch (Exception e) {
			dataServiceLogger.error(e.getMessage(), e);
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_ERROR,
					TaxiTripResponse.TAXI_TRIP_RESPONSE_TEXT_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getaveragefareperminuteforpickuplocationanddayofweek")
	@CrossOrigin()
	public TaxiTripResponse getAverageFarePerMinuteForPickupLocationAndDayOfWeek(
			@RequestParam(value = "pickupLocationId") int pickupLocationId,
			@RequestParam(value = "dayOfWeek") int dayOfWeek) {
		try {
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_SUCCESS,
					taxiTripBusiness.getAverageFarePerMinuteForPickupLocationAndDayOfWeek(pickupLocationId, dayOfWeek));
		} catch (Exception e) {
			dataServiceLogger.error(e.getMessage(), e);
			return new TaxiTripResponse(TaxiTripResponse.TAXI_TRIP_RESPONSE_CODE_ERROR,
					TaxiTripResponse.TAXI_TRIP_RESPONSE_TEXT_ERROR);
		}
	}

}
