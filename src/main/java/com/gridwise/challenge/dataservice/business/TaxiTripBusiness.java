package com.gridwise.challenge.dataservice.business;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;

import com.gridwise.challenge.dataservice.pojo.TaxiTrip;
import com.gridwise.challenge.dataservice.repository.ITaxiTripRepository;
import com.gridwise.challenge.dataservice.util.GenericCsvReaderUtil;

public class TaxiTripBusiness implements ITaxiTripBusiness {
	@Autowired
	private ITaxiTripRepository taxiTripRepository;

	@Override
	public TaxiTrip addTaxiTrip(TaxiTrip taxiTrip) throws Exception {
		return taxiTripRepository.addTaxiTrip(taxiTrip);
	}

	@Override
	public TaxiTrip getTaxiTrip(int taxiTripId) throws Exception {
		return taxiTripRepository.getTaxiTrip(taxiTripId);
	}

	@Override
	public Double getAverageFarePerMileForPickupLocationAndTimePeriod(int pickupLocationId, int periodStart,
			int periodEnd) throws Exception {
		return taxiTripRepository.getAverageFarePerMileForPickupLocationAndTimePeriod(pickupLocationId, periodStart,
				periodEnd);
	}

	@Override
	public Double getAverageFarePerMinuteForPickupLocationAndDayOfWeek(int pickupLocationId, int dayOfWeek)
			throws Exception {
		return taxiTripRepository.getAverageFarePerMileForPickupLocationAndDayOfWeek(pickupLocationId, dayOfWeek);
	}

	@Override
	public Double getAverageFarePerMinuteForPickupLocationAndTimePeriod(int pickupLocationId, int periodStart,
			int periodEnd) throws Exception {
		return taxiTripRepository.getAverageFarePerMinuteForPickupLocationAndTimePeriod(pickupLocationId, periodStart,
				periodEnd);
	}

	@Override
	public Double getAverageFarePerMileForPickupLocationAndDayOfWeek(int pickupLocationId, int dayOfWeek)
			throws Exception {
		return taxiTripRepository.getAverageFarePerMinuteForPickupLocationAndDayOfWeek(pickupLocationId, dayOfWeek);
	}

	@Override
	public void uploadTaxiTrips(InputStream taxiTripInputStream) throws Exception {
		taxiTripRepository.uploadTaxiTrips(GenericCsvReaderUtil.readCsv(TaxiTrip.class, taxiTripInputStream));
	}

}
