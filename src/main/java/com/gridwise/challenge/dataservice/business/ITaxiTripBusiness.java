package com.gridwise.challenge.dataservice.business;

import java.io.InputStream;

import com.gridwise.challenge.dataservice.pojo.TaxiTrip;

public interface ITaxiTripBusiness {
	TaxiTrip addTaxiTrip(TaxiTrip taxiTrip) throws Exception;

	TaxiTrip getTaxiTrip(int taxiTripId) throws Exception;

	Double getAverageFarePerMileForPickupLocationAndTimePeriod(int pickupLocationId, int periodStart, int periodEnd)
			throws Exception;

	Double getAverageFarePerMileForPickupLocationAndDayOfWeek(int pickupLocationId, int dayOfWeek) throws Exception;

	Double getAverageFarePerMinuteForPickupLocationAndTimePeriod(int pickupLocationId, int periodStart, int periodEnd)
			throws Exception;

	Double getAverageFarePerMinuteForPickupLocationAndDayOfWeek(int pickupLocationId, int dayOfWeek) throws Exception;

	void uploadTaxiTrips(InputStream taxiTripInputStream) throws Exception;
}
