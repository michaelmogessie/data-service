docker stop data-service-docker || true
docker rm data-service-docker || true
docker rmi data-service-docker || true
mvn clean package docker:build -DskipTests
docker run -t -p 9997:9997 --name data-service-docker --link docker-mysql data-service-docker
